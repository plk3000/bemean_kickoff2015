'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	http = require('http'),
	url = require('url');

exports.search = function(req, res){
	var post_options = url.parse('http://localhost:9200/people/_search');
	post_options.method = 'POST';
	post_options.headers = {
		'Content-Type' : 'application/json'
	};
	var httpRequest = http.request(post_options, function(response){
		response.setEncoding('utf8');
		var body = '';
		response.on('data', function(d){
			body += d;
		});

		response.on('end', function(){
			var parsed = JSON.parse(body);
			res.jsonp(parsed);
		});
	}).on('error', function(error){
		res.status(500).jsonp(error);
	});

	httpRequest.write(JSON.stringify(req.body));
	httpRequest.end();

	/*http.get(url, function(response){
		var body = '';

		response.on('data', function(d){
			body += d;
		});

		response.on('end', function(){
			var parsed = JSON.parse(body);
			res.jsonp(parsed);
		});
	}).on('error', function(error){
		res.status(500).jsonp(error);
	});*/
};
