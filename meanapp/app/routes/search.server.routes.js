'use strict';

module.exports = function(app) {
	var search = require('../../app/controllers/search.server.controller.js');
	app.route('/search')
		.post(search.search);
};
