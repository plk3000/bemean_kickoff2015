'use strict';

angular.module('todos').directive('todoItem', [
	function() {
		return {
			templateUrl: 'modules/todos/views/todo-item.directive.view.html',
			restrict: 'E'
		};
	}
]);