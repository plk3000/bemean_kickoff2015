'use strict';

//Setting up route
angular.module('people-search').config(['$stateProvider',
	function($stateProvider) {
		// People search state routing
		$stateProvider.
		state('search', {
			url: '/search',
			templateUrl: 'modules/people-search/views/search.client.view.html'
		});
	}
]);