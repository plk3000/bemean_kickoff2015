'use strict';

angular.module('people-search').directive('facet', [
	function() {
		return {
			templateUrl: 'modules/people-search/views/directives/facet.client.view.html',
			restrict: 'E',
			scope: {
				name: '=',
				terms: '=',
				addFilter: '&'
			},
			link: function postLink(scope, element, attrs) {
				var words = scope.name.split('_');
				scope.newName = '';
				words.forEach(function(value){
					switch(value){
						case 'favoriteFruit':
							value = 'favorite Fruit';
							break;
					}
					scope.newName += value.charAt(0).toUpperCase() + value.slice(1).toLowerCase() + ' ';
				});
			}
		};
	}
]);
