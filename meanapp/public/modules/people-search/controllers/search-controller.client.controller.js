'use strict';

angular.module('people-search').controller('SearchController', ['$scope', 'SearchAPI', 'Search',
	function ($scope, SearchAPI, Search) {

		$scope.query = '*';
		var queryPart = {
			'query_string': {
				'query': '*'
			}
		};

		var aggregationsPart = {
			'age': {
				'range': {
					'field': 'age',
					'ranges': [
						{
							'from': 20,
							'to': 25
						},
						{
							'from': 25,
							'to': 30
						},
						{
							'from': 30,
							'to': 35
						},
						{
							'from': 35
						}
					]
				}
			},
			'company': {
				'terms': {
					'field': 'company',
					'size': 5
				}
			},
			'tags': {
				'terms': {
					'field': 'tags',
					'size': 5
				}
			},
			'favoriteFruit': {
				'terms': {
					'field': 'favoriteFruit',
					'size': 5
				}
			}
		};

		var filterPart = {};
		$scope.filters = [];

		$scope.addFilter = function(filter, field){
			filter.field = field;
			$scope.filters.push(filter);
			filterPart = Search.getFilter($scope.filters);
			$scope.doSearch();
		};

		$scope.removeFilter = function(filter){
			for (var i in $scope.filters) {
				if ($scope.filters [i] === filter) {
					$scope.filters.splice(i, 1);
				}
			}
			filterPart = Search.getFilter($scope.filters);
			$scope.doSearch();
		};

		$scope.doSearch = function () {
			var queryObject = {};
			queryPart.query_string.query = $scope.query;
			if($scope.filters.length){
				queryObject = {
					'query': {
						'filtered': {
							'query': queryPart,
							'filter': filterPart
						}
					},
					'aggregations': aggregationsPart
				};
			} else {
				queryObject = {
					'query': queryPart,
					'aggregations': aggregationsPart
				};
			}
			SearchAPI.search(null, queryObject).$promise.then(function (results) {
				$scope.results = results.hits.hits;
				$scope.aggregations = results.aggregations;
				console.log(results);
			});
		};

		$scope.doSearch();
	}
]);
