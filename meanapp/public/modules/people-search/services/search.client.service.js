'use strict';

angular.module('people-search').factory('SearchAPI', ['$resource',
	function ($resource) {
		return $resource('search', {}, {
			search: {
				method: 'POST'
			}
		});
	}
]).service('Search', [function () {
	//var _this = {};
	var filterQuery = {'bool': {}};

	this.getFilter = function (filters) {
		console.log(filters);
		filterQuery.bool.must = [];
		filters.forEach(function(value){
			var filter = {};
			if (value.from !== undefined || value.to !== undefined){
				filter.range = {};
				filter.range[value.field] = {};
				if(value.from !== undefined){
					filter.range[value.field].gte = value.from;
				}
				if(value.to !== undefined){
					filter.range[value.field].lt = value.to;
				}
			} else {
				filter.term = {};
				filter.term[value.field] = value.key;
			}

			filterQuery.bool.must.push(filter);
		});
		return filterQuery;
	};

	//return _this;
}]);
