'use strict';

/**
 * Module dependencies.
 */
require('../models/todo');
var mongoose = require('mongoose'),
	_ = require('lodash'),
	Todo = mongoose.model('Todo');

/**
 * Create a Todo
 */
exports.create = function(req, res) {
	console.log('Server create');
	var todo = new Todo(req.body);
	console.log(todo);

	todo.save(function(err) {
		if (err) {
			console.log('Save failed');
			console.log(err);
			return res.status(400).send(err);
		} else {
			console.log('Save worked');
			res.jsonp(todo);
		}
	});
};

/**
 * Show the current Todo
 */
exports.read = function(req, res) {
	res.jsonp(req.todo);
};

/**
 * Update a Todo
 */
exports.update = function(req, res) {
	var todo = req.todo ;

	todo = _.extend(todo , req.body);

	todo.save(function(err) {
		if (err) {
			return res.status(400).send(err);
		} else {
			res.jsonp(todo);
		}
	});
};

/**
 * Delete an Todo
 */
exports.delete = function(req, res) {
	var todo = req.todo ;

	todo.remove(function(err) {
		if (err) {
			return res.status(400).send(err);
		} else {
			res.jsonp(todo);
		}
	});
};

/**
 * List of Todos
 */
exports.list = function(req, res) { 
	Todo.find({isCompleted: req.query.isCompleted}).sort('-dueDate -created').exec(function(err, todos) {
		if (err) {
			return res.status(400).send(err);
		} else {
			res.jsonp(todos);
		}
	});
};

/**
 * Todo middleware
 */
exports.todoByID = function(req, res, next, id) { 
	Todo.findById(id).exec(function(err, todo) {
		if (err) return next(err);
		if (! todo) return next(new Error('Failed to load Todo ' + id));
		req.todo = todo ;
		next();
	});
};