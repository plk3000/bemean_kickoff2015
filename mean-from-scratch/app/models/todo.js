// app/models/nerd.js
// grab the mongoose module
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Todo Schema
 */
var TodoSchema = new Schema({
	content: {
		type: String,
		default: '',
		required: 'Please fill Todo content',
		trim: true
	},
	isCompleted: {
		type: Boolean,
		default: false
	},
	dueDate: {
		type: Date
	},
	created: {
		type: Date,
		default: Date.now
	}
});

mongoose.model('Todo', TodoSchema);