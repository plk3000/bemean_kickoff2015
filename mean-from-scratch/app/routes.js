// app/routes.js

// grab the nerd model we just created
var todos = require('./controllers/todo.server.controller');

module.exports = function(app) {

    // server routes ===========================================================
    // handle things like api calls
    // authentication routes

    app.route('/_todos')
		.get(todos.list)
		.post(todos.create);

	app.route('/_todos/:todoId')
		.get(todos.read)
		.put(todos.update)
		.delete(todos.delete);

	// Finish by binding the Todo middleware
	app.param('todoId', todos.todoByID);

    // frontend routes =========================================================
    // route to handle all angular requests
    app.get('*', function(req, res) {
        res.sendFile('./public/views/index.html', {root: __dirname + '/../'}); // load our public/index.html file
    });

};