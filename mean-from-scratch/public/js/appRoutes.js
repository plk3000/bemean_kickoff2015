// public/js/appRoutes.js
angular.module('appRoutes', ['ui.router']).config(['$stateProvider', '$locationProvider', '$urlRouterProvider', function($stateProvider, $locationProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider.
    state('home', {
        url: '/',
        templateUrl: 'views/home.html'
    }).
    state('listTodos', {
        url: '/todos',
        templateUrl: 'views/todo.html'
    }).
    state('editTodo', {
        url: '/todos/:todoId/edit',
        templateUrl: 'views/edit-todo.html'
    });

    $locationProvider.html5Mode(true);

}]);