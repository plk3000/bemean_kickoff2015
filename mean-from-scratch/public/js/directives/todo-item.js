angular.module('TodoDirective', []).directive('todoItem',
	function() {
		return {
			templateUrl: 'views/todo-item.html',
			restrict: 'E'
		};
	}
);