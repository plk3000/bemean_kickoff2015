// public/js/controllers/NerdCtrl.js
angular.module('TodoCtrl', []).controller('TodosController', function($scope, $stateParams, $location, Todos) {
    $scope.save = function($event) {
        if ($event.which === 13 && $scope.content) {
            $scope.create();
        }
    };

    // Create new Todo
    $scope.create = function() {
        // Create new Todo object
        var todo = new Todos({
            content: this.content
        });
        $scope.content = '';

        // Redirect after save
        todo.$save(function(response) {
            $scope.find();
        }, function(errorResponse) {
            $scope.error = errorResponse.data.message;
        });
    };

    // Remove existing Todo
    $scope.remove = function(todo) {
        if (todo) {
            todo.$remove();

            for (var i in $scope.incompleteTodos) {
                if ($scope.incompleteTodos[i] === todo) {
                    $scope.incompleteTodos.splice(i, 1);
                }
            }

	        for (var i in $scope.completedTodos) {
		        if ($scope.completedTodos[i] === todo) {
			        $scope.completedTodos.splice(i, 1);
		        }
	        }
        }
        else {
            $scope.todo.$remove(function() {
                $location.path('todos');
            });
        }
    };

    // Update existing Todo
    $scope.update = function(todo) {
        if (!todo) {
            todo = $scope.todo;
        }
        todo.$update(function() {
            $scope.find();
        }, function(errorResponse) {
            $scope.error = errorResponse.data.message;
        });
    };

    // Find a list of Todos
    $scope.find = function() {
        Todos.query({
            isCompleted: false
        }).$promise.then(function(data) {
            $scope.incompleteTodos = data;
        });
        Todos.query({
            isCompleted: true
        }).$promise.then(function(data) {
            $scope.completedTodos = data;
        });
    };

    // Find existing Todo
    $scope.findOne = function() {
        $scope.todo = Todos.get({
            todoId: $stateParams.todoId
        });
    };
});