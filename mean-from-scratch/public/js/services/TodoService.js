// public/js/services/NerdService.js
angular.module('TodosService', ['ngResource']).factory('Todos', ['$resource',
    function($resource) {
        return $resource('_todos/:todoId', {
            todoId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);