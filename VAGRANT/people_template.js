{
  "people_template": {
    "template": "people",
    "settings": {
      "index": {
        "number_of_shards": "1",
        "number_of_replicas": "0"
      }
    },
    "mappings": {
      "people": {
        "properties": {
          "address": {
            "type": "string"
          },
          "gender": {
            "type": "string"
          },
          "greeting": {
            "type": "string"
          },
          "latitude": {
            "type": "double"
          },
          "about": {
            "type": "string"
          },
          "index": {
            "type": "long"
          },
          "registered": {
            "type": "date",
            "format": "YYYY-MM-dd HH:mm:ss ZZ"
          },
          "isActive": {
            "type": "boolean"
          },
          "friends": {
            "properties": {
              "name": {
                "type": "string"
              },
              "id": {
                "type": "long"
              }
            }
          },
          "picture": {
            "type": "string"
          },
          "favoriteFruit": {
            "type": "string"
          },
          "tags": {
            "type": "string"
          },
          "balance": {
            "type": "string"
          },
          "eyeColor": {
            "type": "string"
          },
          "phone": {
            "type": "string"
          },
          "name": {
            "type": "string"
          },
          "guid": {
            "type": "string"
          },
          "company": {
            "type": "string"
          },
          "age": {
            "type": "integer"
          },
          "email": {
            "type": "string"
          },
          "longitude": {
            "type": "double"
          }
        }
      }
    }
  }
}